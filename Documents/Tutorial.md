# What You Will Learn About
In this lesson you will learn about

- what *Vue* is
- how to install *Vue* on your local machine
- how to set up a *Vue* project for a *Single Page Application*
- *Single File Components*

# Vue
## What is Vue?
[Vue](https://vuejs.org) is a frontend *JavaScript* framework for building
Web user interfaces (*UI*).

The homepage of the *Vue* project can be found under

[https://vuejs.org](https://vuejs.org)

### Component-based
Like many other *JavaScript* frameworks, such as *React JS* or *Angular*,
*Vue* is built around the concept of components. You can think of a component
as a new gadget in the *HTML* toolbox. In some way a component defines
a new *HTML* element for a special purpose that you can integrate into
your *HTML* page.

### Declarative Rendering
Just like in most other Web frameworks the aim is to make *HTML*, *CSS* and
*JavaScript* work smoothly together. *JavaScript* is a programming language,
which allows us to write business code. The results of our business code should
be rendered in the *HTML* structure of our Web page. In the browser
*JavaScript* can do that by modifying the *DOM* (= *Object Document Model*),
which is a tree of objects in memory, which represent the structure of what was
found in the original *HTML* code. *Vue* defines a template syntax, which allows
us to easily publish values of our *JavaScript* in the *HTML* document.

**Example**

```
import { createApp } from 'vue'

createApp({
  setup() {
    return {
      hello: 'World'
    }
  }
}).mount('#app')
```
*JavaScript*

```
<div id="app">
  Hello {{ hello }}!
</div>
```
*HTML*

We can access the property `hello`, defined in the *JavaScript* code by
the syntax `{{hello}}` in our *HTML* page.

If we put all this together in an *HTML* file like this

```
<script type="importmap">
  {
    "imports": {
      "vue": "https://unpkg.com/vue@3/dist/vue.esm-browser.js"
    }
  }
</script>

<div id="app">
  Hello {{ hello }}!
</div>

<script type="module">
  import { createApp } from 'vue'

  createApp({
    setup() {
      return {
        hello: 'World'
      }
    }
  }).mount('#app')
</script>
```
*index.html*

### Reactivity
*Reactivity* means that *Vue* tracks state changes in *JavaScript* and
automatically updates the *DOM*.

## Where can Vue be Used?
*Vue* calls itself *The Progressive Framework*. That's because *Vue* was
designed to be used in many different scenarios, such as

- enhancing static HTML pages (no build required)
- building and embedding as *Web Components* on any page*
- creation of Single-Page Application (*SPA*)
- Fullstack / Server-Side Rendering (*SSR*)
- *JAMStack* / Static Site Generation (*SSG*)
- different targets like desktop, mobile, WebGL, terminal etc.

To learn more about the ways, *Vue* can be used, visit
[Ways of using Vue](https://vuejs.org/guide/extras/ways-of-using-vue.html).

## Creating a Vue Project
Since *Vue* is a pure *JavaScript* library, there is no installation required.
You need a *JavaScript* runtime environment, which is usually
[Node](https://nodejs.org). Furthermore you need a text editor or some
IDE, such as [Visual Studio Code](https://code.visualstudio.com/).

With *Node* installed, we can run the *Vue* scaffolding tool as follows

```
npm init vue@latest
```

> `latest` may be replaced by the desired *Vue* version.

Run this tool in the folder, where you want your project set up.
The tool will ask you some questions about the features you want to use
in your application. At the beginning we can go for the defaults.

Let's say you called your project *test-vue*, then the tool creates
a folder called *test-vue* with a basic layout for a *Vue* project.

Now switch to the project folder and run the following command

```
npm install
```

This will install the required libraries, which are located in
the sub-folder *node_modules*.

We can run a development server with the project by

```
npm run dev
```

You will see something like this in your terminal window

![Vite Development Server](./Images/vue-vite-development-server.png)

A local Web server has been started at port 5173. So open a Web browser
and enter `http://localhost:5173/` in the address bar. Then the following
Web application should show up

![Vue Landing Page](./Images/vue-landing-page.png)

> *Vite* is a Web server, which runs the *Vue* application. You can find more
> information about *Vite* under [vitejs.dev](https://vitejs.dev/).
> You can stop the *Vite* server by pressing `Ctrl+C`.

# A Vue Project
Now that we have a *Vue* project running, we will have a short glimpse at
the structure of such a project. Like any *JavaScript* project the main
configuration is located in the *package.json* file. We will quickly go through
the most important folders and files of the project

| Folder/File | Description |
| ----- | -------------------- |
| `package.json` | Main configuration file of any *JavaScript* project. It defines basic project information, dependencies, scripts, etc. |
| `vite.config.js` | The configuration of the *Vite* Web server. |
| `index.html` | The typical landing page of a Web application. |
| `src/main.js` | The code with the mount point for *Vue*. |
| `src/App.vue` | The root component of our application. |
| `src/components` | Further components of our application. |
| `src/assets` | Resource files of the application. |

## Appliction Structure
In order to understand the structure of a *Vue* aplication, we start with
the landing page `index.html` of our application.

```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vite App</title>
  </head>
  <body>
    <div id="app"></div>
    <script type="module" src="/src/main.js"></script>
  </body>
</html>
```

Most important here, are `<div id="app"></div>` and
`<script type="module" src="/src/main.js"></script>`.

`<div id="app"></div>` defines the hook for the root component of our
application. It is identified by the ID `app`.

Secondly the `src/main.js` is loaded by
`<script type="module" src="/src/main.js"></script>`.

Naturally we look at `src/main.js` next

```
import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
```

It creates the root component `App`, located in `src/App.vue` and installs
it at the mount point `#app`, which refers to the ID `app` of the `<div>`
element in the landing page `index.html`.

## Component Structure
We will not go into detail of the component structure here. It is just for
getting a slight idea of what a *Vue* component is.

```
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'
</script>

<template>
  <header>
    <img alt="Vue logo" class="logo" src="./assets/logo.svg" width="125" height="125" />

    <div class="wrapper">
      <HelloWorld msg="You did it!" />
    </div>
  </header>

  <main>
    <TheWelcome />
  </main>
</template>

<style scoped>
header {
  line-height: 1.5;
}

.logo {
  display: block;
  margin: 0 auto 2rem;
}

@media (min-width: 1024px) {
  header {
    display: flex;
    place-items: center;
    padding-right: calc(var(--section-gap) / 2);
  }

  .logo {
    margin: 0 2rem 0 0;
  }

  header .wrapper {
    display: flex;
    place-items: flex-start;
    flex-wrap: wrap;
  }
}
</style>
```

*Vue* follows the idea of putting all into one file. The three technologies
of Web development are

- *HTML*
- *CSS*
- *JavaScript*

`<script>` element contain *JavaScript* code, `<template>` elements *HTML* code
and `<style>` elements *CSS* code. All three together form the code of
a *Vue* component.
